package com.auriseu.autoparkinger;

import com.auriseu.autoparkinger.parkinger.JobExecutor;
import com.auriseu.autoparkinger.util.Constants;
import com.auriseu.autoparkinger.util.PropertyLoader;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Properties;
import java.util.Timer;

public class Main {

    public static void main(String[] args) throws IOException {
        Properties props = PropertyLoader.loadProperties();
        int intervalMinutes = Integer.parseInt(props.getProperty(Constants.PROP_INTERVAL_MINUTES));
        Date now = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());

        JobExecutor job = new JobExecutor(props);
        new Timer().schedule(job, now, intervalMinutes * 60 * 1000);
    }
}
