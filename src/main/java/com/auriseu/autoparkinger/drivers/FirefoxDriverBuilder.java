package com.auriseu.autoparkinger.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class FirefoxDriverBuilder {

    public WebDriver buildWebDriver(String driverName, String driverPath) {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("marionette", false);

        capabilities.setCapability(driverName, driverPath);
        FirefoxDriver firefoxDriver = new FirefoxDriver(capabilities);

        return firefoxDriver;
    }
}
