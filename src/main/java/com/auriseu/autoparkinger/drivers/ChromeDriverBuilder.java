package com.auriseu.autoparkinger.drivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeDriverBuilder {

    public WebDriver buildWebDriver(String driverName, String driverPath) {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(driverName, driverPath);

        ChromeDriver driver = new ChromeDriver(capabilities);

        return driver;
    }
}
