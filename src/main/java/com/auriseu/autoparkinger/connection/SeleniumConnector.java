package com.auriseu.autoparkinger.connection;

import org.openqa.selenium.WebDriver;

public class SeleniumConnector implements Connector {
    private WebDriver driver;
    private String url;

    public SeleniumConnector(WebDriver driver, String url) {
        this.driver = driver;
        this.url = url;
    }


    public WebDriver connect() {
        driver.get(url);

        return driver;
    }

}
