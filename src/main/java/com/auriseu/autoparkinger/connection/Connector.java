package com.auriseu.autoparkinger.connection;

import org.openqa.selenium.WebDriver;

public interface Connector {
    WebDriver connect();
}
