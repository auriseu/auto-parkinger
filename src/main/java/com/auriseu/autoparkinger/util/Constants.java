package com.auriseu.autoparkinger.util;

public class Constants {
    // Property names
    public static final String PROP_USERNAME = "username";
    public static final String PROP_PASSWORD = "password";
    public static final String PROP_INTERVAL_MINUTES = "intervalMinutes";
    public static final String PROP_EXECUTION_WINDOW_START = "executionWindowStart";
    public static final String PROP_EXECUTION_WINDOW_END = "executionWindowEnd";
    public static final String PROP_DRIVER_PATH = "driverPath";
    public static final String PROP_DRIVER_NAME = "driverName";
    public static final String PROP_URL = "url";

    // Messages
    public static final String MSG_STARTING_THE_JOB = ": STARTING THE JOB...";
    public static final String MSG_REJECTED_NON_WORKING_DAY = "Not in execution window or non working day. ";
    public static final String MSG_SUCCESS = "SUCCESS!";
    public static final String MSG_EXECUTOR = "EXECUTOR: ";
    public static final String MSG_ERROR_WHILE_LOGGING_IN = "Error while logging in! \n";
    public static final String MSG_FILTERED_OUT_LOT = "Filtered out lot: ";
    public static final String MSG_FAILED = "FAILED! \n";
    public static final String MSG_NO_LOTS_FOUND = "!!! No lots found !!!";
    public static final String MSG_FINDER = "FINDER: ";
    public static final String MSG_LOT_IS_RESERVED_ALREADY = "Lot is already reserved";
    public static final String MSG_JOB = "JOB: ";

    //HTML
    public static final String HTML_SIGN_IN = "sign-in";
    public static final String HTML_EMAIL = "email";
    public static final String HTML_PASSWORD_ID = "password";
    public static final String HTML_CSS_CLASS_BTN = "btn";
    public static final String PROP_HTML_TAG_BUTTON = "HTML_TAG_BUTTON";
    public static final String PROP_HTML_TAG_TARGET_ELEMENT_AVAILABLE_SPOT = "HTML_TAG_TARGET_ELEMENT_AVAILABLE_SPOT";
    public static final String PROP_HTML_CLASS_BTN_DANGER = "HTML_CLASS_BTN_DANGER";
}
