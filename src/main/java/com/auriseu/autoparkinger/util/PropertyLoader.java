package com.auriseu.autoparkinger.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {
    public static Properties loadProperties() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("resources/config.properties");
            prop.load(input);
        } catch (IOException ex) {
            ConsoleLoger.log("ERROR WHILE LOADING PROPERTIES");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    ConsoleLoger.log("ERROR WHILE LOADING PROPERTIES");
                }
            }
        }
        return prop;
    }
}
