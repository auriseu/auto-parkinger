package com.auriseu.autoparkinger.parkinger;

import com.auriseu.autoparkinger.connection.Connector;
import com.auriseu.autoparkinger.connection.SeleniumConnector;
import com.auriseu.autoparkinger.util.ConsoleLoger;
import com.auriseu.autoparkinger.util.Constants;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.Properties;

public class ParkingerJob {
    public static final int FIVE_SECONDS = 5000;

    private WebDriver driver;
    private LoginResolver resolver;
    private FreeLotsFinder finder;
    private FreeLotsFilter filter;
    private Properties props;

    public ParkingerJob(WebDriver driver, LoginResolver resolver, FreeLotsFinder finder, FreeLotsFilter filter, Properties props) {
        this.driver = driver;
        this.resolver = resolver;
        this.filter = filter;
        this.props = props;
        this.finder = finder;
    }

    public boolean execute() {
        boolean success = false;

        try {
            driver.manage().window().maximize();

            // Login
            driver = connect();
            driver = login();
            // TODO: Invent something better
            Thread.sleep(FIVE_SECONDS);

            // Find all lots
            List<WebElement> freeLots = finder.findFreeLots(driver);

            if (freeLots.isEmpty()) {
                driver.quit();
                return false;
            }

            // Reserve lot
            WebElement freeLotButton = filter.filterOutFreeLot(freeLots);

            if (freeLotButton == null) {
                driver.quit();
                return false;
            }

            try {
                freeLotButton.sendKeys(Keys.PAGE_DOWN);
                Actions actions = new Actions(driver);
                actions.moveToElement(freeLotButton).click().perform();
                success = true;
            } catch (StaleElementReferenceException s) {
                ConsoleLoger.log(Constants.MSG_JOB + Constants.MSG_FAILED + s.getMessage());
                driver.quit();
                return false;
            }

        } catch (RuntimeException e) {
            ConsoleLoger.log(Constants.MSG_JOB + Constants.MSG_FAILED + e.getMessage());
            driver.quit();
            return false;
        } catch (InterruptedException e) {
            ConsoleLoger.log(Constants.MSG_JOB + Constants.MSG_FAILED + e.getMessage());
        }

        driver.quit();
        return success;
    }

    private WebDriver login() {
        return resolver.resolveLogin(driver);
    }

    private WebDriver connect() {
        Connector connector = new SeleniumConnector(driver, props.getProperty(Constants.PROP_URL));
        return connector.connect();
    }
}
