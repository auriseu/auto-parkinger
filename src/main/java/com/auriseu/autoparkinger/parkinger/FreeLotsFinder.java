package com.auriseu.autoparkinger.parkinger;

import com.auriseu.autoparkinger.util.ConsoleLoger;
import com.auriseu.autoparkinger.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class FreeLotsFinder {
    private Properties props;

    public FreeLotsFinder(Properties props) {
        this.props = props;
    }

    public List<WebElement> findFreeLots(WebDriver driver) throws RuntimeException {
        List<WebElement> lots = Collections.EMPTY_LIST;

        try {
            if (!hasAlreadyReservedLot(driver)) {
                lots = driver.findElements(By.tagName(props.getProperty(Constants.PROP_HTML_TAG_TARGET_ELEMENT_AVAILABLE_SPOT)));
            } else {
                ConsoleLoger.log(Constants.MSG_LOT_IS_RESERVED_ALREADY);
                return Collections.EMPTY_LIST;
            }
        } catch (TimeoutException t) {
            ConsoleLoger.log(Constants.MSG_FINDER + Constants.MSG_FAILED + t.getMessage());
        } catch (RuntimeException e) {
            ConsoleLoger.log(Constants.MSG_FINDER + Constants.MSG_FAILED + e.getMessage());
        }

        if (lots == null || lots.isEmpty()) {
            ConsoleLoger.log(Constants.MSG_FINDER + Constants.MSG_NO_LOTS_FOUND);
            lots = Collections.EMPTY_LIST;
        }

        return lots;
    }

    private boolean hasAlreadyReservedLot(WebDriver driver) {
        return !driver.findElements(By.className(props.getProperty(Constants.PROP_HTML_CLASS_BTN_DANGER))).isEmpty();
    }
}
