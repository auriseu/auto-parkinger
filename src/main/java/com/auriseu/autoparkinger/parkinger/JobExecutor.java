package com.auriseu.autoparkinger.parkinger;

import com.auriseu.autoparkinger.drivers.ChromeDriverBuilder;
import com.auriseu.autoparkinger.util.ConsoleLoger;
import com.auriseu.autoparkinger.util.Constants;
import org.openqa.selenium.WebDriver;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Properties;
import java.util.TimerTask;

public class JobExecutor extends TimerTask {
    private Properties props;
    private String userName;
    private String password;
    private LocalTime executionWindowStart;
    private LocalTime executionWindowEnd;
    private String driverPath;
    private String driverName;

    public JobExecutor(Properties props) {
        this.props = props;
        this.userName = props.getProperty(Constants.PROP_USERNAME);
        this.password = props.getProperty(Constants.PROP_PASSWORD);
        this.driverPath = props.getProperty(Constants.PROP_DRIVER_PATH);
        this.driverName = props.getProperty(Constants.PROP_DRIVER_NAME);

        int executionWindowStartHour = Integer.parseInt(props.getProperty(Constants.PROP_EXECUTION_WINDOW_START).substring(0, 2));
        int executionWindowStartMinutes = Integer.parseInt(props.getProperty(Constants.PROP_EXECUTION_WINDOW_START).substring(3));
        int executionWindowEndHour = Integer.parseInt(props.getProperty(Constants.PROP_EXECUTION_WINDOW_END).substring(0, 2));
        int executionWindowEndMinutes = Integer.parseInt(props.getProperty(Constants.PROP_EXECUTION_WINDOW_END).substring(3));

        this.executionWindowStart = LocalTime.of(executionWindowStartHour, executionWindowStartMinutes);
        this.executionWindowEnd = LocalTime.of(executionWindowEndHour, executionWindowEndMinutes);
    }

    @Override
    public void run() {
        if (isInExecutionWindow() && !isNonWorkingDay()) {
            try {
                ConsoleLoger.log(Constants.MSG_EXECUTOR + LocalTime.now().toString() + Constants.MSG_STARTING_THE_JOB);
                WebDriver driver = new ChromeDriverBuilder().buildWebDriver(driverName, driverPath);
                ParkingerJob job = createJob(props, driver);

                if (job.execute()) {
                    ConsoleLoger.log(Constants.MSG_EXECUTOR + Constants.MSG_SUCCESS);
                }
            } catch (Exception e) {
                ConsoleLoger.log(Constants.MSG_EXECUTOR + e.getMessage());
            }
        } else {
            ConsoleLoger.log(Constants.MSG_EXECUTOR + Constants.MSG_REJECTED_NON_WORKING_DAY + LocalTime.now().toString());
        }

        return;
    }

    private boolean isNonWorkingDay() {
        DayOfWeek currentDayOfWeek = LocalDate.now().getDayOfWeek();
        return currentDayOfWeek == DayOfWeek.SATURDAY || currentDayOfWeek == DayOfWeek.SUNDAY;
    }

    private boolean isInExecutionWindow() {
        return LocalTime.now().isAfter(executionWindowStart) && LocalTime.now().isBefore(executionWindowEnd);
    }

    // TODO: Dependency injection tool could be used
    private ParkingerJob createJob(Properties props, WebDriver driver) {
        LoginResolver resolver = new LoginResolver(userName, password);
        FreeLotsFilter filter = new FreeLotsFilter(props);
        FreeLotsFinder finder = new FreeLotsFinder(props);
        return new ParkingerJob(driver, resolver, finder, filter, props);
    }
}
