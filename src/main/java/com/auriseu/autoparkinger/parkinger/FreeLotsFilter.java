package com.auriseu.autoparkinger.parkinger;

import com.auriseu.autoparkinger.util.ConsoleLoger;
import com.auriseu.autoparkinger.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class FreeLotsFilter {
    public static final int FIRST_INDEX = 0;

    private Properties props;

    public FreeLotsFilter(Properties props) {
        this.props = props;
    }

    public WebElement filterOutFreeLot(List<WebElement> freeLots) {
        Collections.reverse(freeLots);
        WebElement selected = freeLots.get(FIRST_INDEX).findElement(By.tagName(props.getProperty(Constants.PROP_HTML_TAG_BUTTON)));
        ConsoleLoger.log(Constants.MSG_FILTERED_OUT_LOT + selected.getText());

        return selected;
    }
}
