package com.auriseu.autoparkinger.parkinger;

import com.auriseu.autoparkinger.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginResolver {
    private String userName;
    private String password;

    public LoginResolver(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public WebDriver resolveLogin(WebDriver driver) throws RuntimeException {
        String currentUrl = driver.getCurrentUrl();

        if (currentUrl.endsWith(Constants.HTML_SIGN_IN)) {
            return login(driver);
        } else {
            throw new RuntimeException(Constants.MSG_ERROR_WHILE_LOGGING_IN);
        }
    }

    private WebDriver login(WebDriver actDriver) {
        actDriver.findElement(By.id(Constants.HTML_EMAIL)).sendKeys(userName);
        actDriver.findElement(By.id(Constants.HTML_PASSWORD_ID)).sendKeys(password);
        actDriver.findElement(By.className(Constants.HTML_CSS_CLASS_BTN)).click();

        return actDriver;
    }
}
